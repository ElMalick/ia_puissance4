import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;

public class Puissance extends Pane{
	Boule boules[][] ;
	int joueur1 = 1, joueur2 = 2;
	int joueur ;
	boolean solo;
	int ligne = 6 ;
	int colonne = 7 ;
	Label label = new Label("Coisissez un mode" );
	int nbreDeTour = 0;
	boolean gameOver = false;  
	int N = 4;
	IaPuissance ia;
	boolean tour ;
	
	Rectangle r = new Rectangle(0, 0, 700, 600);
	
	/**
	 * Constructeur
	 * crée les composantes graphiques
	 */
	public Puissance() {
		 
		joueur = joueur1 ;
		
		r.setFill(Color.BLUE);
		for(int i = 0 ; i < ligne ; i++){
			for (int j = 0 ; j < colonne ; j++){
				Circle c = new Circle(5 +45 + 100*j, 5+45 + 100*i, 45);
				c.setFill(Color.BLUE);
				c.radiusProperty().bind(r.heightProperty().divide(12).subtract(5));
				c.centerXProperty().bind(r.widthProperty().divide(7).multiply(j+0.5));
				c.centerYProperty().bind(r.heightProperty().divide(6).multiply(i+0.5));
				this.getChildren().add(c);
			}
		} //fin du for(int i = 0 ; i < ligne ; i++) 
		
		boules = new Boule[7][6];
		
		
		for(int i = 0 ; i < ligne ; i++){
			for (int j = 0 ; j < colonne ; j++){ 
				boules[j][i] = new Boule();
				boules[j][i].layoutXProperty().bind(r.widthProperty().divide(7).multiply(j));
				boules[j][i].layoutYProperty().bind(r.heightProperty().divide(6).multiply(i));
				boules[j][i].fitHeightProperty().bind(r.heightProperty().divide(6));
				boules[j][i].fitWidthProperty().bind(r.widthProperty().divide(7));
				this.getChildren().add(boules[j][i]);
			}
		}//fin for
		
		label.setFont(Font.font("Comic Sans MS", 600 / 20));
		label.setLayoutX(200);
		label.setLayoutY(635);
		//label.layoutYProperty().bind(r.heightProperty());
		this.getChildren().addAll(label);
		
		//cadres de s�lections:
		Rectangle[] rects = new Rectangle[colonne];
		for(int i = 0 ; i < colonne ; i++){
			rects[i] = new Rectangle(0, 0, 10, 10);
			rects[i].layoutXProperty().bind(r.widthProperty().divide(colonne).multiply(i));
			rects[i].heightProperty().bind(r.heightProperty());
			rects[i].widthProperty().bind(r.widthProperty().divide(colonne));
			rects[i].setFill(Color.TRANSPARENT);
			rects[i].setStroke(Color.GREENYELLOW);
			rects[i].setStrokeType(StrokeType.INSIDE);
			rects[i].setStrokeWidth(12);
			rects[i].setVisible(false);
			
			this.getChildren().addAll(rects[i]);
		}
		
		Rectangle r2 = new Rectangle(0,0,10,10);
		r2.heightProperty().bind(r.heightProperty());
		r2.widthProperty().bind(r.widthProperty());
		r2.setFill(Color.TRANSPARENT);
		this.getChildren().addAll(r2);
		

		nbreDeTour = 1;
		
		
		//clique:
		if (gameOver == false)
				r2.setOnMouseClicked(e -> {
	
					int colonne;
					if(solo) {
						if(tour) {
							colonne = ouJouer(e);
							if (puisJeJouer(colonne)) {							
								jouer(this.boules, colonne);
							
								letTour();
								ia.jouer();
							}
						}
					}
					else {
						colonne = ouJouer(e);
						if (puisJeJouer(colonne)) {							
							jouer(this.boules, colonne);
							
						}
					}
							
				});  
		
	}//fin du constructeur
	
	/**
	 * renvoie la colonne sur laquelle un joueur a cliqué
	 * @param even
	 * @return
	 */
	public int ouJouer(MouseEvent even) {
		int colonne = (int)(even.getX() / (r.getWidth() / this.colonne));
		return colonne;
	}
	
	/**
	 * vérifie si il est possible de jouer sur une colonne
	 * 
	 * @param colonne
	 * @return vraie si on peut et faut sinon
	 */
	public boolean puisJeJouer(int colonne) {
		return ((boules[colonne][0].getJoueur() == 0) && !gameOver);
	}
	
	/**
	 * renvoie la ligne à laquelle on va jouer
	 * @param colonne
	 * @return
	 */
	public int getLigne(int colonne) {
		int rang = ligne-1;
		while(boules[colonne][rang].getJoueur() != 0){
			rang--;
		}
		return rang ;
	}
	
	public int score(int colonne, int rang) {
		
		int couleur = (joueur == joueur1 ? 1 : 2);
		//nombre align�s maximal: 
		int max = 0;
		int x; int y;
		int somme;
		
		//-->  diagonale HG-BD
		x = colonne; y = rang; somme=-1;
		while(y >= 0 && x >= 0 && boules[x][y].getJoueur() == couleur){ y--; x--; somme++;}
		x = colonne; y = rang;
		while(y < this.ligne && x < this.colonne && boules[x][y].getJoueur() == couleur){ y++; x++; somme++;}
		if(somme > max) max= somme;
		
		//-->  diagonale HD-BG
		x = colonne; y = rang; somme=-1;
		while(y >= 0 && x < this.colonne && boules[x][y].getJoueur() == couleur){ y--; x++; somme++;}
		x = colonne; y = rang;
		while(y < this.ligne && x >= 0 && boules[x][y].getJoueur() == couleur){ y++; x--; somme++;}
		if(somme > max) max= somme;
		
		//-->  verticale:
		x = colonne; y = rang; somme=-1;
		while(y >= 0 && boules[x][y].getJoueur() == couleur){ y--; somme++;}
		y = rang;
		while(y < this.ligne && boules[x][y].getJoueur() == couleur){ y++; somme++;}
		if(somme > max) max= somme;
		
		//-->  horizontale:
		x = colonne; y = rang; somme=-1;
		while(x >= 0 && boules[x][y].getJoueur() == couleur){ x--; somme++;}
		x = colonne;
		while(x < this.colonne && boules[x][y].getJoueur() == couleur){ x++; somme++;}
		if(somme > max) max= somme;
		
		return max;
	}
	
	/**
	 * permet de jouer sur une colomnne
	 * @param col
	 * @param lign
	 */
	public int jouer(Boule boules[][] ,int colonne){
		
		    int rang = getLigne(colonne);
			boules[colonne][rang].setJoueur(joueur == joueur1 ? 1 : 2);
			
			int score = score(colonne,rang);
			
			if(score >= N){
				label.setText("victoire du joueur "+joueur);
				gameOver = true;
			}
			else
				label.setText("au tour du joueur "+joueur);
			
				if (joueur == joueur1) joueur = joueur2;
				else joueur = joueur1;
				
			return score;	
		}
	
	/**
	 * renvoie nombre de boules successif de méme couler sur la diagonale principale
	 * @param colonne
	 * @param ligne
	 */
	public int nbDiagonale1(int colonne, int ligne) {
		int x,y,somme ;
		x = colonne; y = ligne; somme=-1;
		while((y >= 0) && (x >= 0) && (boules[x][y].getJoueur() == boules[ligne][colonne].getJoueur())){ y--; x--; somme++;}
		x = colonne; y = ligne;
		while((y < this.ligne) && (x < this.colonne) && (boules[x][y].getJoueur() == boules[ligne][colonne].getJoueur())){ y++; x++; somme++;}
		return somme ;
	}
	
	/**
	 * renvoie nombre de boules successif de méme couler sur la diagonale secondaire
	 * @param colonne
	 * @param ligne
	 */
	public int nbDiagonale2(int colonne, int ligne) {
		int x,y;
		x = colonne; y = ligne; int somme=-1;
		while((y >= 0) && (x < this.colonne) && (boules[x][y].getJoueur() == boules[ligne][colonne].getJoueur())){ y--; x++; somme++;}
		x = colonne; y = ligne;
		while((y < this.ligne) && (x >= 0) && (boules[x][y].getJoueur() == boules[ligne][colonne].getJoueur())){ y++; x--; somme++;}
		return somme;
	}
	
	/**
	 * renvoie nombre de boules successif de méme couler sur une ligne
	 * @param colonne
	 * @param ligne
	 * @return
	 */
	public int nbDiagonaleH(Integer colonne, Integer ligne) {
		int x,y;
		x = colonne; y = ligne; int somme=-1;
		while((x >= 0) && (boules[x][y].getJoueur() == boules[ligne][colonne].getJoueur())){ x--; somme++;}
		x = colonne;
		while((x < this.colonne) && (boules[x][y].getJoueur() == boules[ligne][colonne].getJoueur())){ x++; somme++;}
		return somme ;
	}
	
	/**
	 * renvoie nombre de boules successif de méme couler sur une colonne
	 * @param colonne
	 * @param ligne
	 * @return
	 */
	public int nbDiagonaleV(Integer colonne, Integer ligne) {
		int x,y;
		x = colonne; y = ligne; int somme=-1;
		while((y >= 0) && (boules[x][y].getJoueur() == boules[ligne][colonne].getJoueur())){ y--; somme++;}
		y = ligne;
		while((y < this.ligne) && (boules[x][y].getJoueur() == boules[ligne][colonne].getJoueur())){ y++; somme++;}
		return somme;
	}

	/**
	 * récupérer l'instance de l'IA contre laquelle le joueur va jouer
	 * @param ia
	 */
	public void setIa(IaPuissance ia) {
			this.ia = ia;
	}

	/**
	 * permet au joueur de jouer
	 */
	public void getTour() {
		this.tour = true;
	}
	
	/**
	 * empéche au joueur de jouer
	 */
	public void letTour(){
	this.tour = false;
	}
	
	/**
	 * fixe le mode de la partie
	 */
	public void setMode() {
		this.solo = true;
	}

}
