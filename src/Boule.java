

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Boule extends ImageView {
	
	private static Image joueur1 = new Image("/ressources/7.png");
	private static Image joueur2 = new Image("/ressources/IM1.png");
	
	
	/**
	 * 0 pour un case vide 1 pour le joueur1 et 2 pour joueur2 
	 */
	private int joueur;
	
	public Boule(){
		this.joueur = 0;
	}
	
	/**
	 * place une boule sur une case
	 * @param j
	 */
	public void setJoueur(int j){
		this.setImage(j == 1 ? joueur1 : joueur2);
		this.joueur = j;
	}
	
	/**
	 * renvoie le joueur occupant une case
	 * @return
	 */
	public int getJoueur(){
		return joueur;
	}
	
}
