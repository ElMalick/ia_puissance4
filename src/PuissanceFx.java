import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class PuissanceFx extends Application {
	Boolean gameOver = false;

	@Override
	public void start(Stage stage) throws Exception {
		// TODO Auto-generated method stub
	
		stage.setTitle("temps entre deux click");	    
		
		Scene scene;
		
		Puissance partie = new Puissance();
		IaPuissance ia = new IaPuissance(partie,1);
		
		MainPane mode = new MainPane();
		partie.getChildren().add(mode);
		
		scene = new Scene(partie,700,700);
		
		mode.solo.setOnAction(even->{
			
			partie.setIa(ia);
			ia.setMode();
		
			partie.getChildren().remove(mode); 
			
			ia.jouer();
			
		});
		
		mode.dual.setOnAction(even->{
			partie.getChildren().remove(mode); 
		});
		
		stage.setScene(scene);
		stage.show();
			
	}
	
   public static void main(String[] args) {
	   launch(args);
}
}
