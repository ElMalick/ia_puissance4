import java.util.Random;

public class IaPuissance {
	Puissance partie;
	
	int level ;
	int colonne;
	Boolean tour;
	
	/**
	 * constructeur
	 * @param partie
	 * @param level
	 */
	public IaPuissance(Puissance partie, int level) {
		this.partie = partie ;
		this.level = level ;
	}
	

	
	/**
	 * 
	 * @param tab
	 * @return
	 */
	/*public int minMax(Boule boules[][]) {
		int colonne;
		
		TreeSearch tree = new TreeSearch(new State(partie.boules),0); 
		
		State state;
		partie.boules.clone();
		for(int i = 0 ; i < 7 ; i++) {
			
			state = new State(partie.boules.clone());
			partie.jouer(state.state, i);
			tree.addChild(new TreeSearch(state,1), 1);
		}
		
		
		return 0;
	}*/

	
	/**
	 * permet à l'IA de jouer
	 */
	public void jouer() {		
		Random r = new Random();
		int colonne = r.nextInt(7);
		//partie.jouer(colonne);	
		
				partie.jouer(partie.boules, colonne);
				partie.getTour();
	
	}
	
	/**
	 * fixe le mode de la partie
	 */
	public void setMode() {
		this.partie.setMode();
	}

}
