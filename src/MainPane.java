import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;


public class MainPane extends Pane {
	private Rectangle r;
	public Button solo, dual;
	
	public MainPane() {
		
		r = new Rectangle(700, 600);
		r.setFill(Color.DARKBLUE);
		
		solo = new Button("MAN Vs IA");
		solo.setLayoutX(300);
		solo.setLayoutY(200);
		
		dual = new Button("MAN Vs MAN");
		dual.setLayoutX(300);
		dual.setLayoutY(400);
		
		
		this.getChildren().add(r);
		this.getChildren().addAll(solo,dual);
	}
	  

}
